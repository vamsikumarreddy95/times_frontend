import logo from './logo.svg';
import './App.css';

import HeaderAppBar from './components/header';
import Body from './components/body';

function App() {
  return (
    <div >
     <HeaderAppBar/>
     <Body/>
    </div>
  );
}

export default App;
