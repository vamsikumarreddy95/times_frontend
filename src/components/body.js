import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
      display:'inline-block',
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  content:{
    display:'inline-block',
    fontSize:"25px",
    
  }
}));

const data=[
    {
    "title": "Amy Schneider’s Jeopardy! Streak Ends at 40 Consecutive Wins and $1.4 Million",
    "link": "https://time.com/6142934/amy-schneider-jeopardy-streak-ends/"
    },
    {
    "title": "'Writing With Fire' Shines a Light on All-Women News Outlet",
    "link": "https://time.com/6142628/writing-with-fire-india-documentary/"
    },
    {
    "title": "Moderna Booster May Wane After 6 Months",
    "link": "https://time.com/6142852/moderna-booster-wanes-omicron/"
    },
    {
    "title": "Pressure Mounts for Biden to Nominate a Black Woman to the Supreme",
    "link":
    "https://time.com/6142743/joe-biden-supreme-court-nominee-black-woman-campaignpromise/"
    },
    {
    "title": "The James Webb Space Telescope Is in Position—And Now We Wait",
    "link": "https://time.com/6142769/james-webb-space-telescope-reaches-l2/"
    },
    {
    "title": "We Urgently Need a New National COVID-19 Response Plan",
    "link": "https://time.com/6142718/we-need-new-national-covid-19-response-plan/"
    }
    ]

export default  function Body() {
  const [latestStories,setLatestStories]=useState([])
  const classes = useStyles();

  useEffect(()=>{
    getAllData();
  },[])
         const getAllData=()=>{
                 axios.get('http://localhost:5000/getTimeStories').then(res=>{
                  // console.log(res.data)
                   setLatestStories(res.data.data)
                 }).catch(err=>{
                  console.log(err)
                 })
         }
  return (
    <div className={classes.root}>
     
      <Grid container style={{marginTop:"15px"}}>
      <Grid item xs={8}>
      <Grid container spacing={3} >
      <Grid item xs={2}>
        </Grid>
        <Grid item xs={10} >
        <Typography style={{marginTop:"20px",fontWeight:"30px",fontSize:"30px",fontFamily: 'Josefin Sans'}}>FEATURED VOICES</Typography>  
        </Grid>
        <Grid item xs={2}>
          {/* <Paper className={classes.paper}></Paper> */}
        </Grid>
        <Grid item xs={5} className={classes.content}>
          <Typography style={{color:"red",fontFamily: 'Josefin Sans'}}>Anatol Leiven</Typography>
          <Typography style={{marginTop:"7px", letterSpacing:"1px",fontFamily: 'Josefin Sans'}}>Russia Has Been Warning the West Abbout Ukraine for Decades.</Typography>
        
        </Grid>
        <Grid item xs={5} className={classes.content}>
          <Typography style={{color:"red",fontFamily: 'Josefin Sans'}}>Ismart Ara</Typography>
          <Typography style={{marginTop:"7px", letterSpacing:"1px",fontFamily: 'Josefin Sans'}}>How It Feels to Be a Muslim Woman Sold by India's Right Wing</Typography>
        
        </Grid>


        <Grid item xs={2}>
          {/* <Paper className={classes.paper}></Paper> */}
        </Grid>
        <Grid item xs={5} className={classes.content}>
          <Typography style={{color:"red",fontFamily: 'Josefin Sans'}}>Robert J.Devis</Typography>
          <Typography style={{marginTop:"7px", letterSpacing:"1px",fontFamily: 'Josefin Sans'}}>Why You Shouldn't Exercise to Lose Weight</Typography>
        
        </Grid>
        <Grid item xs={5} className={classes.content}>
          <Typography style={{color:"red",fontFamily: 'Josefin Sans'}}>Yohanca Delgado</Typography>
          <Typography style={{marginTop:"7px", letterSpacing:"1px",fontFamily: 'Josefin Sans'}}>The Life-Changing Practice That Will Help You Feel More Gratitude</Typography>
        
        </Grid>


        <Grid item xs={2}>
          {/* <Paper className={classes.paper}></Paper> */}
        </Grid>
        <Grid item xs={5} className={classes.content}>
          <Typography style={{color:"red",fontFamily: 'Josefin Sans'}}>W.Kamau Bell</Typography>
          <Typography style={{marginTop:"7px",fontFamily: 'Josefin Sans', letterSpacing:"1px"}}>Thee's So Much More To Say About Bill Cosby</Typography>
        
        </Grid>
        <Grid item xs={5} className={classes.content}>
          <Typography style={{color:"red",fontFamily: 'Josefin Sans'}}>Chrisina Bu</Typography>
          <Typography style={{marginTop:"7px",fontFamily: 'Josefin Sans', letterSpacing:"1px"}}>What Norway Can Teach the World About Switching to Electric Vehicles</Typography>
        
        </Grid>
        

        
        </Grid>
      </Grid>
      <Grid item xs={1}></Grid>
      <Grid item xs={3}>
      <Grid container spacing={3} style={{backgroundColor:"yellow"}}>
      <Grid item xs={12} style={{textAlign:"center"}}>
      <Typography style={{fontSize:"18px",fontWeight:"30px",fontFamily: 'Josefin Sans'}}>LATEST STORIES</Typography>
      </Grid>
         {latestStories.length && latestStories.map((x,index)=>{return(<>
         <Grid item xs={2}></Grid>
        <Grid item xs={8} className={classes.content}  >
  
                    <a href={x.link}  target="_blank" style={{fontSize:"18px",fontFamily: 'Josefin Sans',textDecoration:"none",color:"black"}}>
                    <Typography  >{x.title}</Typography>
                    </a>
          {/* <Typography style={{fontSize:"18px",fontFamily: 'Josefin Sans'}} >{x.title}</Typography> */}
          <Typography  style={{fontSize:"12px",fontFamily: 'Josefin Sans'}}>{x.date}</Typography>
        
        </Grid>
        <Grid item xs={2}></Grid>
        </>)})}
      </Grid>  
      </Grid>
      </Grid>
    </div>
  );
}
